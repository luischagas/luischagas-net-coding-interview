﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SecureFlight.Api.Models;
using SecureFlight.Api.Utils;
using SecureFlight.Core;
using SecureFlight.Core.Entities;
using SecureFlight.Core.Interfaces;

namespace SecureFlight.Api.Controllers;

[ApiController]
[Route("[controller]")]
public class FlightsController : SecureFlightBaseController
{
    private readonly IService<Flight> _flightService;
    private readonly IService<Passenger> _passengerService;
    private readonly IService<PassengerFlight> _passengerFlightService;

    public FlightsController(IService<Flight> flightService, IMapper mapper, IService<Passenger> passengerService, IService<PassengerFlight> passengerFlightService)
        : base(mapper)
    {
        _flightService = flightService;
        _passengerService = passengerService;
        _passengerFlightService = passengerFlightService;
    }

    [HttpGet]
    [ProducesResponseType(typeof(IEnumerable<FlightDataTransferObject>), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorResponseActionResult))]
    public async Task<IActionResult> Get()
    {
        var flights = await _flightService.GetAllAsync();
        return GetResult<IReadOnlyList<Flight>, IReadOnlyList<FlightDataTransferObject>>(flights);
    }
    
    [HttpPost("{flightId/passengerId}")]
    [ProducesResponseType(typeof(IEnumerable<FlightDataTransferObject>), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorResponseActionResult))]
    public async Task<IActionResult> AddPassengerFlight(long flightId, string passengerId)
    {
        var flight = await _flightService.FilterAsync(f => f.Id == flightId);

        if (flight.Result.FirstOrDefault() is null)
        {
            return new ErrorResponseActionResult
            {
                Result = new ErrorResponse
                {
                    Error = new Error
                    {
                        Code = ErrorCode.NotFound,
                        Message = $"No flight was found {flightId}"
                    }
                }
            };
        }
        
        var passenger = await _passengerService.FilterAsync(p => p.Id == passengerId);

        if (passenger.Result.FirstOrDefault() is null)
        {
            return new ErrorResponseActionResult
            {
                Result = new ErrorResponse
                {
                    Error = new Error
                    {
                        Code = ErrorCode.NotFound,
                        Message = $"No passenger was found {passengerId}"
                    }
                }
            };
        }
        
        var result = await _passengerFlightService.AddAsync(new PassengerFlight()
        {
            FlightId = flightId,
            PassengerId = passengerId
        });


        if (result.Succeeded)
            return Ok(result);
        
        return new ErrorResponseActionResult
        {
            Result = new ErrorResponse
            {
                Error = new Error
                {
                    Code = ErrorCode.InternalError,
                    Message = $"Error to add passenger to flight - passengerId {passengerId} - flightId {flightId}"
                }
            }
        };
    }
    
}